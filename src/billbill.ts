// You can also use the generator at https://skeleton.dev/docs/generator to create these values for you
import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';
export const billbill: CustomThemeConfig = {
	name: 'billbill',
	properties: {
		// =~= Theme Properties =~=
		'--theme-font-family-base': `'Roboto Condensed', Inter, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'`,
		'--theme-font-family-heading': `'Roboto Condensed', Inter, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'`,
		'--theme-font-color-base': '0 0 0',
		'--theme-font-color-dark': '255 255 255',
		'--theme-rounded-base': '2px',
		'--theme-rounded-container': '2px',
		'--theme-border-base': '1px',
		// =~= Theme On-X Colors =~=
		'--on-primary': '255 255 255',
		'--on-secondary': '255 255 255',
		'--on-tertiary': '0 0 0',
		'--on-success': '255 255 255',
		'--on-warning': '255 255 255',
		'--on-error': '255 255 255',
		'--on-surface': '255 255 255',
		// =~= Theme Colors  =~=
		// primary | #2C3E50
		'--color-primary-50': '223 226 229', // #dfe2e5
		'--color-primary-100': '213 216 220', // #d5d8dc
		'--color-primary-200': '202 207 211', // #cacfd3
		'--color-primary-300': '171 178 185', // #abb2b9
		'--color-primary-400': '107 120 133', // #6b7885
		'--color-primary-500': '44 62 80', // #2C3E50
		'--color-primary-600': '40 56 72', // #283848
		'--color-primary-700': '33 47 60', // #212f3c
		'--color-primary-800': '26 37 48', // #1a2530
		'--color-primary-900': '22 30 39', // #161e27
		// secondary | #4080e7
		'--color-secondary-50': '226 236 251', // #e2ecfb
		'--color-secondary-100': '217 230 250', // #d9e6fa
		'--color-secondary-200': '207 223 249', // #cfdff9
		'--color-secondary-300': '179 204 245', // #b3ccf5
		'--color-secondary-400': '121 166 238', // #79a6ee
		'--color-secondary-500': '64 128 231', // #4080e7
		'--color-secondary-600': '58 115 208', // #3a73d0
		'--color-secondary-700': '48 96 173', // #3060ad
		'--color-secondary-800': '38 77 139', // #264d8b
		'--color-secondary-900': '31 63 113', // #1f3f71
		// tertiary | #BDC3C7
		'--color-tertiary-50': '245 246 247', // #f5f6f7
		'--color-tertiary-100': '242 243 244', // #f2f3f4
		'--color-tertiary-200': '239 240 241', // #eff0f1
		'--color-tertiary-300': '229 231 233', // #e5e7e9
		'--color-tertiary-400': '209 213 216', // #d1d5d8
		'--color-tertiary-500': '189 195 199', // #BDC3C7
		'--color-tertiary-600': '170 176 179', // #aab0b3
		'--color-tertiary-700': '142 146 149', // #8e9295
		'--color-tertiary-800': '113 117 119', // #717577
		'--color-tertiary-900': '93 96 98', // #5d6062
		// success | #1A7440
		'--color-success-50': '221 234 226', // #ddeae2
		'--color-success-100': '209 227 217', // #d1e3d9
		'--color-success-200': '198 220 207', // #c6dccf
		'--color-success-300': '163 199 179', // #a3c7b3
		'--color-success-400': '95 158 121', // #5f9e79
		'--color-success-500': '26 116 64', // #1A7440
		'--color-success-600': '23 104 58', // #17683a
		'--color-success-700': '20 87 48', // #145730
		'--color-success-800': '16 70 38', // #104626
		'--color-success-900': '13 57 31', // #0d391f
		// warning | #ce840d
		'--color-warning-50': '248 237 219', // #f8eddb
		'--color-warning-100': '245 230 207', // #f5e6cf
		'--color-warning-200': '243 224 195', // #f3e0c3
		'--color-warning-300': '235 206 158', // #ebce9e
		'--color-warning-400': '221 169 86', // #dda956
		'--color-warning-500': '206 132 13', // #ce840d
		'--color-warning-600': '185 119 12', // #b9770c
		'--color-warning-700': '155 99 10', // #9b630a
		'--color-warning-800': '124 79 8', // #7c4f08
		'--color-warning-900': '101 65 6', // #654106
		// error | #BE1D1D
		'--color-error-50': '245 221 221', // #f5dddd
		'--color-error-100': '242 210 210', // #f2d2d2
		'--color-error-200': '239 199 199', // #efc7c7
		'--color-error-300': '229 165 165', // #e5a5a5
		'--color-error-400': '210 97 97', // #d26161
		'--color-error-500': '190 29 29', // #BE1D1D
		'--color-error-600': '171 26 26', // #ab1a1a
		'--color-error-700': '143 22 22', // #8f1616
		'--color-error-800': '114 17 17', // #721111
		'--color-error-900': '93 14 14', // #5d0e0e
		// surface | #20304c
		'--color-surface-50': '222 224 228', // #dee0e4
		'--color-surface-100': '210 214 219', // #d2d6db
		'--color-surface-200': '199 203 210', // #c7cbd2
		'--color-surface-300': '166 172 183', // #a6acb7
		'--color-surface-400': '99 110 130', // #636e82
		'--color-surface-500': '32 48 76', // #20304c
		'--color-surface-600': '29 43 68', // #1d2b44
		'--color-surface-700': '24 36 57', // #182439
		'--color-surface-800': '19 29 46', // #131d2e
		'--color-surface-900': '16 24 37' // #101825
	}
};
