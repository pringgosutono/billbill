import { readable, writable } from 'svelte/store';

export const pageTitle = writable<string>('');

export const tblStyle = readable({
	container: {
		'border-radius': 0,
		// 'font-size': '12px',
		padding: '0px 1px'
	},
	table: {
		'border-radius': 0
	},
	th: {
		'border-radius': 0,
		// 'vertical-align': 'middle',
		padding: '10px'
		// 'font-size': '12px'
	},
	td: {
		// 'vertical-align': 'top',
		padding: '5px 10px'
		// 'font-size': '12px',
		// 'line-height': '1.25rem'
	},
	header: {
		'border-radius': 0
	},
	footer: {
		'border-radius': 0,
		padding: '0.5rem'
	}
});
