import { join } from 'path';
import type { Config } from 'tailwindcss';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';
import { skeleton } from '@skeletonlabs/tw-plugin';
import { billbill } from './src/billbill';
import plugin from 'tailwindcss/plugin';

export default {
	darkMode: 'class',
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		join(require.resolve('@skeletonlabs/skeleton'), '../**/*.{html,js,svelte,ts}')
	],
	theme: {
		screens: {
			sm: '480px',
			md: '768px',
			lg: '976px',
			xl: '1440px'
		},
		// spacing: {
		// 	sm: '8px',
		// 	md: '12px',
		// 	lg: '16px',
		// 	xl: '24px'
		// },

		extend: {}
	},
	plugins: [
		forms,
		typography,
		skeleton({
			themes: {
				custom: [billbill]
			}
		}),
		plugin(function ({ addBase }) {
			addBase({
				html: { fontSize: '16px' }
			});
		})
	]
} satisfies Config;
